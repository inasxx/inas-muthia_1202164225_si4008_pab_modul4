package com.example.inasmuthia_1202164225_si4008_pab_modul4;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class Login extends AppCompatActivity {

    TextView register;
    EditText vEmail, vPassword;
    Button masuk;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        register = findViewById(R.id.txtDaftar);
        vEmail = findViewById(R.id.inputMail);
        vPassword = findViewById(R.id.inputPass);
        masuk = findViewById(R.id.btnMasuk);

        if (FirebaseAuth.getInstance().getCurrentUser() != null){
            startActivity(new Intent(Login.this, MainActivity.class));
            finish();
        }


        masuk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String mMail = vEmail.getText().toString().trim();
                String pPass = vPassword.getText().toString().trim();
                if (mMail.isEmpty()){
                    Toast.makeText(Login.this, "Email harus diisi", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (pPass.isEmpty()){
                    Toast.makeText(Login.this, "Password harus diisi", Toast.LENGTH_SHORT).show();
                    return;
                }
                FirebaseAuth.getInstance().signInWithEmailAndPassword(mMail,pPass).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()){
                            startActivity(new Intent(Login.this, MainActivity.class));
                            finish();
                        }
                    }
                });
            }
        });

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Login.this, Register.class));
            }
        });

    }
}
