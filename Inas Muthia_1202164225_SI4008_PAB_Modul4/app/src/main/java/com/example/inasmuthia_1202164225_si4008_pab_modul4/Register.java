package com.example.inasmuthia_1202164225_si4008_pab_modul4;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class Register extends AppCompatActivity {

    TextView login;
    EditText vName, vEmail, vPass;
    Button daftar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        if (FirebaseAuth.getInstance().getCurrentUser() != null){
            startActivity(new Intent(Register.this, MainActivity.class));
            finish();
        }

        login = findViewById(R.id.txtLogin);

        vName = findViewById(R.id.name);
        vEmail = findViewById(R.id.email);
        vPass = findViewById(R.id.password);
        daftar = findViewById(R.id.btnDaftar);

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Register.this, Login.class));
            }
        });

        daftar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               String mMail = vEmail.getText().toString().trim();
                String pPass = vPass.getText().toString().trim();
                if (vName.getText().toString().isEmpty()){
                    Toast.makeText(Register.this, "Nama harus diisi", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (mMail.isEmpty()){
                    Toast.makeText(Register.this, "Email harus diisi", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (pPass.isEmpty()){
                    Toast.makeText(Register.this, "Password harus diisi", Toast.LENGTH_SHORT).show();
                    return;
                }
                FirebaseAuth.getInstance().createUserWithEmailAndPassword(mMail,pPass).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()){
                            startActivity(new Intent(Register.this, MainActivity.class));
                            finish();
                        }
                    }
                });
            }
        });
    }
}
